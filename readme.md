# ACF Additions and Improvements
This plugin adds some small but useful additions and improvements to the Advanced Custom Fields Pro plugin.

## Features:
- Add prepend icons to input text fields:
  - Calendar icon (-calendar)
  - Clock icon (-time)
  - Euro icon (-euro)
  - Dollar icon (-dollar)
  - Mail icon (-mail)
  - LinkedIn icon (-linkedin)
  - Pinterest icon (-pinterest)
  - Google icon (-google)
  - Facebook icon (-facebook)
  - Instagram icon (-instagram)
  - Twitter icon (-twitter)
  - Youtube icon (-youtube)
  - Vimeo icon (-vimeo)


## Installation:

Add repository to your local composer file:

    {
        "type": "vcs",
        "url": "git@bitbucket.org:joldnl/jold-acf-additions.git"
    }



Add the plugin as a depenency:

    "joldnl/jold-acf-additions": "~0.2",


Update composer by running:

    $ composer update.



## Instructions:
A simple way to prepend custom predefined icons to acf custom field.

To place a custom icon left in the input field, copy and paste the folowing code into the 'prepend' field of a acf custom field. Replace the `[-dollar]` with one of the new classes.

```
<i class="acf-icon acfadd-icon -dollar small"></i>
```

The icons used in this plugin can be edited via Fontastic:
http://app.fontastic.me/#select/U3XBAmDhRBGBGtXFegp2SE


## HTML Code to Prepend:

**Calendar Icon:**
`<i class="acf-icon acfadd-icon -calendar small"></i>`


**Clock/time Icon:**
`<i class="acf-icon acfadd-icon -time small"></i>`


**Euro Icon:**
`<i class="acf-icon acfadd-icon -euro small"></i>`


**Dollar Icon:**
`<i class="acf-icon acfadd-icon -dollar small"></i>`


**Mail Icon:**
`<i class="acf-icon acfadd-icon -mail small"></i>`


**LinkedIn Icon:**
`<i class="acf-icon acfadd-icon -linkedin small"></i>`


**Pinterest Icon:**
`<i class="acf-icon acfadd-icon -pinterest small"></i>`


**Pushpin Icon:**
`<i class="acf-icon acfadd-icon -pushpin small"></i>`


**Google Icon:**
`<i class="acf-icon acfadd-icon -google small"></i>`


**Facebook Icon:**
`<i class="acf-icon acfadd-icon -facebook small"></i>`


**Instagram Icon:**
`<i class="acf-icon acfadd-icon -instagram small"></i>`


**Twitter Icon:**
`<i class="acf-icon acfadd-icon -twitter small"></i>`


**Youtube Icon:**
`<i class="acf-icon acfadd-icon -youtube small"></i>`


**Vimeo Icon:**
`<i class="acf-icon acfadd-icon -vimeo small"></i>`



## Screenshot
![ACF Additions fields screenshot](https://bytebucket.org/joldnl/jold-acf-additions/raw/4ea119fa11735a46851ce71d1b67c304261c0b8a/screenshot.png "Screenshot")
