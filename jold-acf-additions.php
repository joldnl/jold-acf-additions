<?php
/*
    Plugin Name:            ACF: Additions
    Description:            Adds some small but usefull additions and improvements to the Advanced Custom Fields Pro plugin.
    Version:                0.4.0
    Plugin URI:             https://bitbucket.org/joldnl/jold-acf-additions/
    Bitbucket Plugin URI:   https://bitbucket.org/joldnl/jold-acf-additions
    Bitbucket Branch:       master
    Author:                 Jurgen Oldenburg
    Author URI:             http://www.jold.nl
*/


define( "ACFADD_PLUGIN_FOLDER", dirname( __FILE__ ) );
define( "ACFADD_PLUGIN_DIR", plugins_url( '', __FILE__ ) );
define( "ACFADD_PLUGIN_NAME", "acf-additions" );
define( "ACFADD_PLUGIN_TEXTDOMAIN", "acfadditions") ;
define( "ACFADD_PLUGIN_DBPREFIX", "acfadd_" );
define( "ACFADD_PLUGIN_VERSION", "0.2.8" );



/**
 * Main ACF Additions Class
 */
new JoldAcfAdditions;
class JoldAcfAdditions {

    /**
     * Construct to fire functions on load
     */
    function __construct() {
        add_action( 'admin_init', array( $this, 'load_assets') );
    }



    /**
     * Add plugin textdomain translations
     * @return none
     */
    function textdomain() {
        load_plugin_textdomain( ACFADD_PLUGIN_TEXTDOMAIN, FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
    }



    /**
    * Register and load assets on plugin pages
    * @param  string $hook WordPress Hook
    * @return none
    */
    function load_assets( $hook ) {
        // Load style plugin styles
        wp_register_style( ACFADD_PLUGIN_NAME . '-main', ACFADD_PLUGIN_DIR . '/assets/css/jold-acf-additions-iconfont.css', '', ACFADD_PLUGIN_VERSION, 'all' );
        wp_enqueue_style( ACFADD_PLUGIN_NAME . '-main');
    }

}
